# Aufgabe

Die Yamyams leben in einer aus Quadraten bestehenden Welt.
Jedes Quadrat ist entweder ein Hindernis, ein Zielfeld oder ein freies Feld. [Randmauer]
Zu jedem Zeitpunkt befindet sich ein Yamyam auf einem Quadrat, welches kein Hindernis ist.
Zu jedem freien Quadrat gibt es eine Menge von vier Nachfolgern.
Dabei bestimmt man den Nachfolger in Richtung R (hoch, runter, links, rechts), indem man in diese Richtung läuft, bis man auf ein Hindernis stößt.
Das Feld davor ist der Nachfolger in Richtung R.
<!--![Richtungen](../../raw/master/richtungen.svg)-->
Entsprechend kann man einen Vorgänger definieren: a ist genau dann Vorgänger von b, wenn b Nachfolger von a ist.

In jedem Zeitschritt wählt ein Yamyam zufällig eine Himmelsrichtung aus und springt auf den Nachfolger in dieser Richtung. Wenn es auf einem Zielfeld landet, bleibt es dort stehen.

Ein Feld heißt sicher, wenn ein Yamyam von diesem Feld aus unabhängig davon, wie die zufälligen Bewegungen ausfallen, auf einem Zielfeld landen wird.
Aufgabe ist es, für eine gegebene Welt alle sicheren Felder zu bestimmen.

# Lösungsidee

Ein Feld heißt sicher, wenn ein Yamyam von diesem Feld aus unabhängig davon, wie die zufälligen Bewegungen ausfallen, auf einem Zielfeld landen wird. Wir nennen ein nicht sicheres Feld gefährlich. Ein Feld ist gefährlich, wenn es von dort aus möglich ist, auf lange Sicht nicht auf einem Zielfeld zu landen. Auf lange Sicht landet man genau dann nicht auf einem Zielfeld, wenn man auf einem Feld landet, von dem aus es unmöglich ist, auf einem Zielfeld zu landen; ein solches heiße hoffnungslos. Ein Feld, von dem aus es möglich ist, auf einem Zielfeld zu landen, heiße entsprechend hoffnungsvoll.

*Venndiagramm*

Die Lösung kann man nun folgendermaßen berechnen:
 1. Ein Feld, von dem aus man ein Zielfeld erreichen kann, ist hoffnungsvoll.
 2. Ein freies Feld, das nicht hoffnungsvoll ist, ist hoffnungslos.
 3. Ein Feld, von dem aus man ein hoffnungsloses Feld erreichen kann, ist gefährlich.
 4. Ein freies Feld, das nicht gefährlich ist, ist sicher. Außerdem sind alle Zielfelder sicher.

Um 1. und 3. sauber zu definieren, benötigt man den Begriff der Erreichbarkeit. Diesen kann man rekursiv definieren:
 * Von Feld a ist a erreichbar.
 * Wenn c Vorgänger von b ist und a von b erreichbar ist, ist a von c erreichbar.

Diese Definition kann man mithilfe des Flooding-Algorithmus umsetzen. Es geht darum, alle Felder zu finden, von denen ein beliebiges Feld f der Menge F erreichbar ist. Dann gilt:
 * Die Menge E enthält alle Felder e, von denen wir wissen, dass ein f von e erreichbar ist.
 * Die Menge R (genannt "Flooding-Rand") enthält alle Felder r, die erreichbar sind, aber eventuell Vorgänger haben, die noch nicht untersucht wurden.
 * Anfangs enthalten beide Mengen die Elemente von F.
 * In jedem Schritt wird ein Element r aus R ausgewählt. Für jeden Vorgänger von r, genannt v:
   * v wird in E eingefügt. (Laut Rekursion ist ein f von v erreichbar.)
   * Wenn v vorher nicht in E enthalten war, wird es zu R hinzugefügt. (Wenn v in E enthalten war, wurde es bereits vorher zu R hinzugefügt.)
 * Anschließend wird r aus R entfernt, denn all seine Vorgänger wurden nun untersucht.

Da dieser Algorithmus so oder in ähnlicher Form bekannt ist, wird auf einen Beweis seiner Richtigkeit verzichtet.

Nun fehlt nur noch eine einzige Vorraussetzung: ein Algorithmus, der die Vorgänger eines Feldes a berechnet.
a ist genau dann Nachfolger von b in nördlicher Richtung, wenn:
 * a nördlich von b liegt
 * das Feld direkt nördlich von a ein Hindernis ist (sonst könnte kein Yamyam bei a anhalten)
 
Demnach muss man bestimmen, ob direkt nördlich von a ein Hindernis liegt. Ist dies der Fall, sind alle freien Felder, die in Folge südlich von a liegen, Vorgänger von a. Dies wiederholt man für die anderen drei Himmelsrichtungen.

Mithilfe dieser Algorithmen kann man die Lösung nun zusammensetzen.

## Begründung: Ein Feld ist genau dann sicher, wenn es keine Möglichkeit gibt, eine Sackgasse zu erreichen
Eine Sackgasse ist eine Menge von Feldern, von der aus es nicht möglich ist, einen Ausgang zu erreichen.
Um die Aussage zu beweisen, beweisen wir zwei Teilaussagen:
 1. Wenn von einem Feld aus eine Sackgasse erreicht werden kann, ist es nicht sicher.
 2. Wenn von einem Feld aus keine Sackgasse erreicht werden kann, ist es sicher.
 
### Beweis der ersten Teilaussage:
P(S) sei die Wahrscheinlichkeit, eine Sackgasse zu erreichen. Da dies möglich ist, ist P(S) > 0. P(A) sei die Wahrscheinlichkeit, einen Ausgang zu erreichen. Diese Ereignisse schließen sich offenbar aus, daher ist P(S \cut A) = 0. Es gilt:  
1 >= P(S \unity A) = P(S) + P(A) - P(S \cut A) = P(S) + P(A)  
P(A) <= 1 - P(S) < 1  
Damit ist es nicht sicher, einen Ausgang zu erreichen.

### Beweis der zweiten Teilaussage:
*Bild mit Kringeln*  
*Sackgassenformulierung nach oben übertragen*

A sei die Menge der Ausgänge. Die freien Felder werden in zwei Teilmengen unterteilt: die Sackgassenfelder S, von denen man nicht nach A gelangen kann, und die hoffnungsvollen Felder H, von denen man nach A gelangen kann.  
Für jedes Feld h aus H kann man eine natürliche Zahl e und eine positivie reelle Zahl p so definieren, dass die Wahrscheinlichkeit, nach e Zügen zu einem Zielfeld gelangt zu sein, mindestens p beträgt. 
