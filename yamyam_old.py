
def addvec(p1, p2):
	(x1, y1) = p1
	(x2, y2) = p2
	return ((x1+x2), (y1+y2))

def subvec(p1, p2):
	(x1, y1) = p1
	(x2, y2) = p2
	return ((x1-x2), (y1-y2))

legend = {
	'#': 1,
	' ': 0,
	'E': 2,
}

revlegend = {
	0: ' ',
	1: '#',
	2: 'E',
}

blegend = {
	True: '#',
	False:' '
}


#boardString =  ["############",
#				"# #      # #",
#				"#   ##     #",
#				"#   #   E# #",
#				"#       ## #",
#				"#          #",
#				"# #E    E###",
#				"############"]
boardString = open('yamyams5.txt')
width = 24
height = 24
karte = []

withHope = [[False for x in range(width)] for x in range(height)]
withFear = [[False for x in range(width)] for x in range(height)]

def print_map(m, dic):
	for line in m:
		for char in line:
			print(dic.get(char), end="")
		print(" ")

def mapat(m, p):
	(x, y) = p
	return m[x][y]

def setmap(m, p, v):
	(x, y) = p
	m[x][y] = v

def preceding(pos):
	ret = []
	for step in [(0, 1), (1, 0), (0, -1), (-1, 0)]:
		if mapat(karte, subvec(pos, step)) == 1 or mapat(karte, pos) == 2: # wall before or hole
			nextpos = addvec(pos, step)
			while mapat(karte, nextpos) == 0:
				ret.append(nextpos)
				nextpos = addvec(nextpos, step)
	return ret

def following(pos):
	ret = []
	for step in [(0, 1), (1, 0), (0, -1), (-1, 0)]:
		nextpos = pos
		while mapat(karte, nextpos) != 1:
			nextpos = addvec(nextpos, step)
		ret.append(subvec(nextpos, step))
	return ret
				
		
# initialize karte
for line in boardString:
	row = []
	for char in line.strip():
		row.append(legend.get(char))
	karte.append(row)

print_map(karte, revlegend)

#print_map(withHope)

#print(following((3, 5)))

drains = []
for x, line in enumerate(karte):
	for y, field in enumerate(line):
		if field == 2:
			drains.append((x, y))

print(drains)

toHope = []
toHope.extend(drains)

while toHope:
	hopepos = toHope.pop()
	if not mapat(withHope, hopepos):
		setmap(withHope, hopepos, True)
		next = preceding(hopepos)
		toHope.extend(next)

print_map(withHope, blegend)


hopeless = []
for x, line in enumerate(withHope):
	for y, field in enumerate(line):
		if (not field) and (mapat(karte, (x, y)) == 0):
			hopeless.append((x, y))
print(hopeless)

toFear = []
toFear.extend(hopeless)

while toFear:
	fearpos = toFear.pop()
	if (not mapat(withFear, fearpos)) and mapat(karte, fearpos) != 2:
		setmap(withFear, fearpos, True)
		next = preceding(fearpos)
		toFear.extend(next)

print_map(withFear, blegend)

for x, line in enumerate(karte):
	for y, field in enumerate(line):
		pos = (x, y)
		if field == 0 and not mapat(withFear, pos):
			print('s', end=" ")
		else:
			print(revlegend.get(field), end=" ")
	print(" ")

